This is a repository for a bot for the Lubuntu project.

This repository is currently mirrored on:
 1. GitHub
 2. Launchpad
 3. GitLab
 4. Bitbucket

Under the username tsimonq2.

You need to have the irc and launchpadlib Python libraries installed to use this bot.

Usage: python main.py <server[:port]> <channel> <nickname>

License: DWTFYWT
